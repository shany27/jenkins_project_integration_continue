package client;


public class Client {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        try{
            Registry reg = LocateRegistry.getRegistry(1090);
            Interface c = (Interface) reg.lookup("Rmi");
            int x, y;
            Affichage.afficherMenu();
           // int a = Affichage.lireNombreEntier();
            int choix = Affichage.lireValiderEntier (0,4);
            switch (choix){
                case 1:
                {
                    System.out.println("Entrer x et y");
                    x=sc.nextInt();
                    y=sc.nextInt();
                    System.out.println(c.addition(x,y));
                    break;
                }
                case 2:
                {
                    System.out.println("Entrer x et y");
                    x=sc.nextInt();
                    y=sc.nextInt();
                    System.out.println(c.soustraction(x,y));
                    break;
                }
                case 3:
                {
                    System.out.println("Entrer x et y");
                    x=sc.nextInt();
                    y=sc.nextInt();
                    System.out.println(c.multiplication(x,y));
                    break;
                }
                case 4:
                {
                    System.out.println("Entrer x et y");
                    x=sc.nextInt();
                    y=sc.nextInt();
                    System.out.println(c.division(x,y));
                    break;
                }
                case 5:
                {
                    System.out.println("Entrer x et y");
                    x=sc.nextInt();
                    y=sc.nextInt();
                    System.out.println(c.modulo(x,y));
                    break;
                }
                case 6 :
                {
                    
                    System.out.println("Entrer un nombre pour calculer son factoriel");
                    x=sc.nextInt();
                    System.out.println(c.factoriel(x));
                    
                }
            }
        }catch (Exception e){
            System.out.println("Exception : "+e);
        }

    }

}
