package interfaces;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Calco extends UnicastRemoteObject implements Operations {

    public Calco() throws RemoteException{
        int a,b;
    }

    @Override
    public int addition(int x, int y) throws RemoteException {
        return x+y;
    }

    @Override
    public int soustraction(int x, int y) throws RemoteException {
        return x-y;
    }

    @Override
    public int multiplication(int x, int y) throws RemoteException {
        return x*y;
    }

    @Override
    public int division(int x, int y) throws RemoteException {
        return x/y;
    }

    @Override
    public int modulo(int x, int y) throws RemoteException {
        return x%y;
    }

    @Override
    public int factoriel(int x) throws RemoteException{
        int p=1;
        for(int i=1; i<=x; i++){
            p*=x;
        }
        return p;
    }

    
}
