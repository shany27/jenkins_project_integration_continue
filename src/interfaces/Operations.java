package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Operations extends Remote {

    public int addition(int x, int y) throws RemoteException;
    public int soustraction(int x, int y) throws RemoteException;
    public int multiplication(int x, int y) throws RemoteException;
    public int division(int x, int y) throws RemoteException;
    public int modulo(int x, int y) throws RemoteException;
    public int factoriel(int x) throws RemoteException;

}
