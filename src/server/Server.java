package server;

import interfaces.Calco;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends Calco {

    public Server() throws RemoteException {
    }

    public static void main(String[] args) {
        try{
            Registry reg = java.rmi.registry.LocateRegistry.createRegistry(1090);
            reg.rebind("Rmi", new Calco());
            System.out.println("Serveur connecté... ");
        }catch (Exception e){
            System.out.println("Serveur non connecté "+e);
            e.printStackTrace();
        }
    }

}
